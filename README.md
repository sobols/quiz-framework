# Quiz Framework

A set of tools and helper classes that simplify automatic generation of quiz questions for [iRunner](https://bitbucket.org/sobols/irunner2) platform. The framework helps you to construct JSON files with questions suitable to be uploaded to iRunner.

The framework runs on either Python 2 or Python 3 and has no external dependencies. You do not have to install Django, etc.

## Model

Let us briefly describe the data model of quizzes in iRunner.

### Question

A *question* is a basic data unit. It is defined by its *kind* (one of: single answer, multiple answers, text answer, etc.), a *text* (human-readable statement description) and an ordered list of *choices*. Each choice contains its own *text* and is marked as *right* or *wrong*.

Questions may be created and edited individually from the web interface of iRunner.

In order to maintain data integrity, questions are immutable. When you edit a question, it means that, internally, a modified copy of the question is created and the original question is marked as obsolete (*is_deleted* flag is set in the database). As a result, if a question has ever been shown to a student during a quiz session, its formulation remains unchanged in that quiz.

### Question Group

A *question group* is a set of similar questions. During a quiz, a number of questions is chosen from a question group at random. So, all questions in a group are expected to have almost equal difficulty from the student's point of view.

For example, if the question is to calculate a number of connected components in a graph, then each of the questions in the question group contains its own instance of the graph.

Each question group must have its own unique *name*. Questions within a group usually have the same kind and are built according to the same template (but, in general, this is not required). Also, questions are not enforced to be distinct, duplicates are allowed.

### Category

For simpler navigation, question groups are organized into *categories*. Each group can belong to at most one category.

A category is defined by its full *name* (i. e. "Algorithms and Data Structures") and a so-called *slug* (i. e. "algo"). Slugs are short names written in Latin that are shown in page urls (i. e. "/quizzes/categories/algo/groups/").

## General Pipeline

To make a question set, you choose a proper category (or create a new one, if needed) and set up a new question group in this category. Then, your task is to fill the group with questions.

In general, you can do it manually through the UI creating questions one by one. To speed up filling form fields, an existing question may be cloned and edited.

Nevertheless, in many cases automatic question generation is a better choice. Simple JSON file format is used to upload questions into the given question group *in bulk*. Your JSON should contain a single list of objects.
```
[
	{ /* question 1 */ },
	{ /* question 2 */ },
	{ /* question N */ }
]
```
Each object stands for one question, for example:
```json
{
    "text": "Question text?",
    "type": "single",
    "choices": [
        {
            "text": "first choice",
            "is_right": false
        }, 
        {
            "text": "second choice",
            "is_right": true
        }, 
        {
            "text": "third choice",
            "is_right": false
        }
    ]
}
```

It is up to you how to create such file. We recommend to use this Framework, but you may use a text editor or your favourite programming language.

After uploading a JSON, new questions immediately appear in the question group and are ready to be included into quizzes.

## Using the Framework

First you need to clone this repository.

### Create a Factory

Questions are produced by a *factory*. Each factory is intended to create questions of a certain question group given a state of a random number generator. In Python, a factory is just a class that implements simple `IQuestionFactory` interface which has a single `make_question` method.

Factory classes are located at the **factories** directory. Please put each factory in a separate *.py* file. The class name must be equal to `Factory`.

Each factory is identified by a short Latin name called *a slug*. By default, the name of *.py* file acts like a slug. However, you may override a slug in your class.

Consider an example of creating a factory. Choose a descriptive slug for your new factory (we will use **myfactory**) and create a Python file named **myfactory.py** in the **factories** directory. You may start with the following template (Python 2):
```python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .factory import IQuestionFactory
from .question import ConcreteQuestion


class Factory(IQuestionFactory):
    name = 'Human-readable name of question group'

    def make_question(self, rng):
        q = ConcreteQuestion()
        q.text = 'How do you do?'
        return q
```

You need to set the `name` and write the implementation of the `make_question` method. It receives the `rng`, instance of `random.Random` class, and returns a `Question` instance.

### Compose a Question

`Question` is an abstract class, your factory should create an instance of one of its derived classes (`SingleAnswerQuestion`, `MultipleAnswersQuestion`, `TextAnswerQuestion`) and fill it with data. iRunner supports the following types of quiz questions.

#### Single answer
This kind of questions means that a student is asked to choose a single correct item from the list of given choices.

A question must contain at least one choice. Exactly one choice must be marked as correct.

Use `SingleAnswerQuestion` class to create such question from a factory:
```python
from .question import SingleAnswerQuestion

q = SingleAnswerQuestion()
q.text = 'Question text?'
q.add_choice('first choice', False)
q.add_choice('second choice', True)
q.add_choice('third choice', False)
```

This code generates the following JSON:
```json
{
    "text": "Question text?",
    "type": "single",
    "choices": [
        {
            "text": "first choice",
            "is_right": false
        }, 
        {
            "text": "second choice",
            "is_right": true
        }, 
        {
            "text": "third choice",
            "is_right": false
        }
    ]
}
```

#### Multiple answers
Here, at least one choice must be marked as correct; there may be more than one correct items.

Class `MultipleAnswersQuestion` has the same interface as `SingleAnswerQuestion`:
```python
from .question import MultipleAnswersQuestion

q = MultipleAnswersQuestion()
q.text = 'Question text?'
q.add_choice('first choice', False)
q.add_choice('second choice', True)
q.add_choice('third choice', True)
```

#### Text answer

Class `TextAnswerQuestion` has the same interface:
```python
from .question import TextAnswerQuestion

q = TextAnswerQuestion()
q.text = 'Question text?'
q.set_answer('correct answer')
```

Text-answer questions are represented in JSON as follows:
```json
{
    "text": "Question text?",
    "type": "text",
    "choices": [
        {
            "text": "correct answer",
            "is_right": true
        }
    ]
}
```
Note that a single choice is provided and it is marked as right.

#### Examples

Refer to the files at the **factories** directory as examples:

* **maxnumber.py** — single-answer question;
* **primenumbers.py** — multiple-answer question;
* **countedges.py** — text-answer question.

### Test a Factory

When your factory is ready, you want to check that it produces valid and diverse questions. The Framework provides an ability to preview the questions locally in a web browser. Run

```
./main.py myfactory -p
```

This opens an HTML file located at the **preview** directory and shows a set of generated questions (10 by default, you may ask to generate more by passing the `-q` option).

You can modify the factory and try again as many times as you wish.

### Generate Questions to Upload

Run

```
./main.py myfactory -q 100
```

This command generates 100 questions using the specified factory. Questions are stored as JSON to the **generated** subdirectory. Then your task is to upload the file to iRunner through a web form.
