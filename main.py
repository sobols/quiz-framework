from __future__ import print_function

import argparse
import json
import logging
import os
import pkgutil
import random
import webbrowser


def abspath(*args):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *args)


def load_factories():
    import factories
    result = {}
    for importer, modname, ispkg in pkgutil.iter_modules(factories.__path__):
        if ispkg:
            continue
        fullmodname = '{}.{}'.format(factories.__name__, modname)
        m = importer.find_module(fullmodname).load_module(fullmodname)
        factory_cls = getattr(m, 'Factory', None)
        if factory_cls is None:
            logging.warning('module "%s" does not contain Factory class', modname)
            continue
        slug = factory_cls.slug or modname
        factory_cls.slug = slug
        result[slug] = factory_cls()
    return result


def get_cli_args():
    parser = argparse.ArgumentParser(description='Generate questions.')
    parser.add_argument('--preview', '-p', action='store_true', help='preview generated questions in browser')
    parser.add_argument('factories', nargs='*', help='slugs of factories')
    parser.add_argument('--list', '-l', action='store_true', help='print all available factories')
    parser.add_argument('--qnum', '-q', default=10, type=int, help='number of questions to generate for each factory')

    args = parser.parse_args()
    return args


def list_factories(all_factories):
    if len(all_factories) == 0:
        print('No available factories')
        return
    print('Available factories:')
    for slug, fr in all_factories.items():
        print(slug + ':\t' + fr.name)


def select_factories(all_factories, slugs):
    if len(slugs) == 0:
        frs = list(all_factories.values())
    else:
        frs = []
        for slug in slugs:
            factory = all_factories.get(slug)
            if factory is None:
                logging.warning('Unknown factory: %s', slug)
            else:
                frs.append(factory)
    return frs


def json_dumps(obj):
    return json.dumps(obj, ensure_ascii=False, indent=4)


def write_json_file(path, obj):
    with open(path, 'wb') as f:
        f.write(json_dumps(obj).encode('utf-8'))


def main():
    logging.basicConfig(format='%(levelname)s %(message)s', level=logging.DEBUG)

    all_factories = load_factories()

    args = get_cli_args()

    if args.list:
        list_factories(all_factories)
        return

    frs = select_factories(all_factories, args.factories)

    groups = []
    for fr in frs:
        group = {'name': fr.name}
        questions = []
        for i in range(0, args.qnum):
            q = fr.make_question(random.Random(random.randint(0, 10000000)))
            questions.append(q.to_dict())
        write_json_file(abspath('generated', '{}.json'.format(fr.slug)), questions)
        group['questions'] = questions
        groups.append(group)

    if not args.preview:
        return
    with open(abspath('preview', 'data.js'), 'wb') as js:
        data = 'window.groups_data = ' + json_dumps(groups) + ';'
        js.write(data.encode('utf-8'))
    webbrowser.open(abspath('preview', 'index.html'))


if __name__ == '__main__':
    main()
