# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .factory import IQuestionFactory
from .question import SingleAnswerQuestion


class Factory(IQuestionFactory):
    name = 'Максимум'

    def make_question(self, rng):
        n = 1000
        num_answers = 5

        q = SingleAnswerQuestion()
        q.text = 'Из заданных чисел выберите наибольшее.'
        numbers = rng.sample(range(-n + 1, n), num_answers)
        for x in numbers:
            q.add_choice('${}$'.format(x), x == max(numbers))
        return q
