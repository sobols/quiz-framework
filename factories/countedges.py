# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .factory import IQuestionFactory
from .question import TextAnswerQuestion
from .util.adjmatrix import gen_graph


class Factory(IQuestionFactory):
    name = 'Подсчёт рёбер в графе'

    def make_question(self, rng):
        n = 6
        a = gen_graph(rng, n, p=0.4)

        q = TextAnswerQuestion()
        q.text = (
            'Граф задан матрицей смежности $A$. Определите число рёбер в графе.\n'
            '$$\n'
            'A={}\n'
            '$$'
        ).format(a.texify())
        ans = self._solve(a)
        q.set_answer('{}'.format(ans))
        return q

    def _solve(self, a):
        return sum(sum(row) for row in a) // 2
