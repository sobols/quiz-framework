# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .factory import IQuestionFactory
from .question import MultipleAnswersQuestion


def is_prime(x):
    if x == 1:
        return False
    i = 2
    while i * i <= x:
        if x % i == 0:
            return False
        i += 1
    return True


class Factory(IQuestionFactory):
    name = 'Простые числа'

    def make_question(self, rng):
        n = 100
        num_answers = 6

        q = MultipleAnswersQuestion()
        q.text = 'Укажите числа, которые являются простыми.'

        while True:
            num_primes = 0
            for x in rng.sample(range(1, n), num_answers):
                x_is_prime = is_prime(x)
                q.add_choice('${}$'.format(x), x_is_prime)
                num_primes += x_is_prime

            if not (num_primes == 0 or num_primes == num_answers):
                break
            q.clear_choices()

        return q
