class IQuestionFactory(object):
    slug = None
    name = None

    def make_question(self, rng):
        raise NotImplementedError()
