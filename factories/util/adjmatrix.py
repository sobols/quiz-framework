# -*- coding: utf-8 -*-

from __future__ import unicode_literals

'''
Matrix with 1-based indexing
'''


class Row(object):
    __slots__ = 'data'

    def __init__(self, n, default=0):
        self.data = [default for _ in range(n)]

    def __getitem__(self, ndx):
        if ndx == 0:
            raise IndexError('use 1-based indexing')
        return self.data[ndx - 1]

    def __setitem__(self, ndx, value):
        if ndx == 0:
            raise IndexError('use 1-based indexing')
        self.data[ndx - 1] = value

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        for x in self.data:
            yield x

    def texify(self):
        return ' & '.join(_item_to_tex(x) for x in self.data)


class AdjacencyMatrix(object):
    def __init__(self, n, default=0):
        self.n = n
        self.a = [Row(n, default) for _ in range(n)]

    def __getitem__(self, ndx):
        return self.a[ndx - 1]

    def texify(self):
        return (
            '\\begin{pmatrix}\n' +
            '\\\\\n'.join(row.texify() for row in self) +
            '\n\\end{pmatrix}'
        )

    def __iter__(self):
        for row in self.a:
            yield row

    def vertices(self):
        return range(1, self.n + 1)


def _item_to_tex(x):
    if x is None:
        return '\\infty'
    else:
        return '{}'.format(x)


'''
Erdős–Rényi random graph model
'''


def gen_graph(rng, n, p=0.5):
    a = AdjacencyMatrix(n)
    for u in range(1, n + 1):
        for v in range(u + 1, n + 1):
            a[u][v] = a[v][u] = int(rng.random() < p)
    return a


def gen_digraph(rng, n, p=0.5):
    a = AdjacencyMatrix(n)
    for u in range(1, n + 1):
        for v in range(1, n + 1):
            if u != v:
                a[u][v] = int(rng.random() < p)
    return a


def gen_weighted_graph(rng, n, p=0.5, mx=10):
    a = AdjacencyMatrix(n)
    for u in range(1, n + 1):
        for v in range(u + 1, n + 1):
            a[u][v] = a[v][u] = rng.randint(1, mx) if rng.random() < p else None
    return a
